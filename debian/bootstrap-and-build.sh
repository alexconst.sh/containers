#!/bin/sh

# License: GPLv2 / GPLv3
# Copyright: Oleksandr Hnatiuk (aka Alex Const)

# Builds a local debian:buster image using debootstrap and any of
# {podman,buildah,docker} (first one available, in this order)

set -e

fail() {
    printf '%s\n' "$1" >&2
    exit 1
}

choose_build_cmd() (
    unalias -a

    [ -n "$BUILD_CMD" ] || BUILD_CMD="$(command -v podman) build"
    [ -n "$BUILD_CMD" ] || BUILD_CMD="$(command -v buildah) bud"
    [ -n "$BUILD_CMD" ] || BUILD_CMD="$(command -v docker) build"

    if [ -n "$BUILD_CMD" ] ; then
        echo "$BUILD_CMD"
    else
        fail "No container engine / builder detected"
    fi
)

script_dir="$(dirname "$(realpath "$0")")"

test -x /usr/sbin/debootstrap || fail "No debootstrap executable found"

while getopts "o:" opt ; do
case "$opt" in
        o)
                output_dir="$OPTARG"
                ;;
        *)
                fail "No such flag: $opt"
                ;;
esac
done

output_dir="${output_dir:-"$(pwd)/buster"}"

CONTAINER_BUILD_CMD="$(choose_build_cmd)"

sudo debootstrap buster "$output_dir" https://deb.debian.org/debian
(cd "$output_dir" && sudo tar -cf "$script_dir/buster.tar" -- *)
sudo chown "$(id -u):$(id -g)" "$script_dir/buster.tar"
$CONTAINER_BUILD_CMD -t debian:buster "$script_dir"
